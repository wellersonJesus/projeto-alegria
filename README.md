# **[Projeto Alegria Get-Portugal](https://projeto-alegria-wellersonjesus-d1b08c2b252f1a78fd1fb8d27b2df06c.gitlab.io)**

## O que é o Projeto Alegria?
O **Projeto Alegria** é uma iniciativa cristã da igreja Get-Portugal focada em promover o evangelismo e o caracter firmado na palavra de Deus, através de brincadeiras, jogos e atividades culturais, sociais e educacionais. Nosso objetivo é unir membros da igreja, crianças jovens e adolescentes de 2 a 25 anos para celebrar a cristo e construir uma comunidade mais firmada no evangelio.

## Como posso participar do projeto?
Você pode participar do Projeto Alegria de diversas formas! Inscreva-se como voluntário, participe das nossas atividades e eventos, ou contribua com suas ideias e talentos. Visite a nossa seção "[Informações Gerais](informacoes/informacoes_geral.md)" para saber mais sobre as oportunidades e regras disponíveis.

## Qual é o tema do projeto para este ano?
O tema deste ano é **_"Conexões Humanas - Frutos do espírito"_**. Focaremos em fortalecer os laços entre as pessoas, promovendo empatia, cooperação e entendimento mútuo através de uma série de eventos e atividades temáticas.

## Tem alguma premiação para os participantes?
Sim! Os participantes terão a chance de ganhar prêmios em várias categorias, como primeiro, segundo e terceiro lugar. Além disso, todos as equipes participantes receberão pontos de participação e reconhecimento pelo seu envolvimento no projeto.

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>

