# **Bem-vindo ao Projeto Alegria Get-Portugal**

O Projeto Alegria Get-Portugal é uma iniciativa que visa promover a união, diversão e competição saudável entre todos os participantes. 

## Qual é o tema do projeto para este ano?
O tema deste ano é **_"Conexões Humanas - Frutos do espírito"_**. Focaremos em fortalecer os laços entre as pessoas, promovendo empatia, cooperação e entendimento mútuo através de uma série de eventos e atividades temáticas.


## Instruções

O evento começa na sexta-feira, 18/10, para crianças a partir de 9 anos, adolescentes e jovens. Você pode dormir na igreja ou chegar no sábado, 19/10, até 9h30 para participar da gincana. Traga o necessário para aproveitar ao máximo!

Não esqueça de levar:  
> Barraca  
> Travesseiro, colchonete e cobre leito  
> Lanterna  
> Roupas extras (camisa, bermuda/calça, chinelo)  
> Tênis  
> Produtos de higiene pessoal (escova de dentes, creme dental, creme de pentear/gel, perfume)  
> Protetor solar  
> Toalha  
> Squeeze ou garrafinha de água  
> Documentos com foto  


## Tem alguma premiação para os participantes?
Sim! Os participantes terão a chance de ganhar o prêmio como<br> 
>**_Primeiro lugar_** - **_Segundo lugar_** - **_Terceiro lugar_** 

As equipes que demonstrarem maior envolvimento nos seguintes quesitos serão pontuadas:

>Colaboração - 10 pontos<br>
>Criatividade - 10 pontos<br>
>Companherismo - 10 pontos<br>
>Participação - 10 pontos<br>
>Comprometimento - 7 pontos<br>
>Envolvimento - 7 pontos<br>
>Comunicação - 5 pontos<br>
>Empatia - 5 pontos<br>

Todos estes critérios serão avaliados continuamente pelo corpo técnico, durante todo o projeto. Cada equipe poderá receber apenas 01 (uma) única avaliação por quesito.

<div style="text-align: right;">
    <span style="color: #25736d; display: inline-block; text-align: left; font-family: 'Playwrite USA Traditional', sans-serif; font-size: 16px; font-style: italic;">
        "...Vós me ensinareis o caminho da vida, há abundância de alegria junto de vós, <br>e delícias eternas à vossa direita."<br>Salmos 16:11
    </span>
</div>

## Limpeza e manutenção de todo espaço do evento
Para garantir a limpeza eficaz do espaço, o Projeto Alegria conta com a colaboração de todos os envolvidos. A participação de todos é essencial para ajudar na limpeza completa do local. O projeto se encerrará no dia 19/10/2024 às 16:00, mas a responsabilidade pela limpeza recai sobre todos os "Apoiadores", "Animadores", "Apresentadores", "Juízes" e a "Equipe Técnica". Somente após a conclusão da limpeza é que seremos liberados.

É importante que todos estejam cientes deste compromisso para que possamos nos organizar e trabalhar com empenho e dedicação até o final do evento. Vale lembrar que na noite do dia 19/10, a igreja retomará sua programação normal, e não podemos comprometer o andamento das atividades.

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_
</sub>
