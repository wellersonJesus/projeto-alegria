# **Setores locais dos eventos**

Os eventos ocorrerão em seis áreas principais: 
>Templo<br>
>Espaço Gourmet<br>
>Quadra<br>
>Campo<br>
>Geração Santa<br>
>Espaço Memórias<br>

Cada local contará com atividades específicas para diferentes faixas etárias, proporcionando uma experiência diversificada e organizada para todos os participantes.

## Cronograma 18.10.2024 (Sexta-feira)
!!! success "**Sexta-feira**"   
    - Abertura: Abertura dos portões **19:40**
    - Espaço Memórias: Recepção, Identificação **19:40 - 21:00** 
    - Estacionamento: Montagem barracas **19:40 - 21:00**

!!! success "**Templo**"    
    - Vigília (Banda - Agápe) **21:00 - 22:00**
    - Karaokê (Banca de Jurados) **22:00 - 23:00**
    - Divisão de Equipes **23:30 - 00:00**
    - Prova relâmpago: Desafio de Charadas **00:00 - 00:30**

!!! success "**Espaço Gourmet**"
    - Jantar, fogueira e "causos" **00:50 ...**

## Cronograma 19.10.2024 (Sábado)
!!! success "**Templo**"
    - Devocional - Todos equipe - **08:00 hrs**
    - Grito de Guerra - Toda equipe - **10:00 hrs**
    - Caça ao Tesouro (2 a 5) **10:15 hrs**

!!! success "**Espaço memórias**"
    - Jogos - Livre - **11:00 - 13:00 hrs**
    - Jogos - Livre - **14:00 - 15:00 hrs**

!!! success "**Espaço gourmet**"
    - Café da manhã - **07:00 hrs**
    - Almoço - **13:00 hrs**
    - Lanche da tarde - **15:30 hrs**

!!! success "**Quadra**"
    - Volleyball (9 a 25) - **14:00 hrs**

!!! success "**Campo**"
    - Empinar pipa - Toda equipe - **10:15 hrs**
    - Bola na toalha (6 a 9) **15:05 hrs**
    - Corrida (9 a 13) **15:05 hrs**

!!! success "**Geração santa**"
    - Balde de água (14 a 17) **12:00 hrs**
    - Jogo da velha no chão (9 a 13) **14:00 hrs**
    - Juntando as peças (2 a 5) **14:00 hrs**
    - Desafio da Múmia (6 a 8) **15:00 hrs**

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>

