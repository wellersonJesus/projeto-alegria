# **Encerramento**

O encerramento do Projeto Alegria Get-Portugal marca o fim de uma jornada repleta de diversão, aprendizado e camaradagem. 

Será um momento especial para reconhecer o esforço e a dedicação de todos os participantes, equipes e colaboradores que tornaram este evento possível. 

Juntos, celebraremos as conquistas alcançadas e os momentos memoráveis compartilhados ao longo da gincana. Este será um momento para refletir sobre as experiências vividas e fortalecer os laços que foram criados durante esta experiência única de união e alegria.

Estamos ansiosos para celebrar com todos vocês no encerramento deste evento tão especial!

>**Dia:** _19.10.2024 ás 16:00_<br>
>**Local:** _Av. Portugal, 300 - Jardim Atlântico._

## Importante
Para garantir a limpeza eficaz do espaço da Igreja Get-Portugal, a colaboração de todos os envolvidos no Projeto Alegria é essencial, conforme descrito no manual de regras no item "[Manual de regaras - 4.2.2.14](http://127.0.0.1:8000/informacoes/regras/)"   

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
