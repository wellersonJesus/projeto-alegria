# Manual de Regras - Projeto Alegria Get-Portugal

O manual de regras do Projeto Alegria é claro e objetivo, fornecendo orientações essenciais para todos os envolvidos. 

!!! tip "**Regras**"
    - **1 Visão geral**<br>
        1.1 Promova a união e a celebração cristã através de atividades culturais, sociais e educacionais para crianças e jovens de 2 a 25 anos.<br>
        1.2 Participe como voluntário, envolva-se nas atividades e eventos, ou contribua com ideias e talentos para o Projeto Alegria.<br>
        1.3 Fomente conexões humanas e fortaleça os laços comunitários promovendo empatia, cooperação e entendimento mútuo.<br>
    - **2 Agenda**<br>
        - **2.1 Agenda inscriçõe**<br>
        2.1.1 O prazo para inscrição é até 08.10.2024 às 22:00 horas.<br>
        2.1.2 Luciana, Giceli e Cristiane são responsáveis pela coordenação das inscrições.<br>
        - **2.2 Agenda Cronograma**<br>
        2.2.1 As atividades começarão com a recepção, identificação e montagem de barracas das 21:00 às 22:00 na sexta-feira.<br>
        2.2.2 A vigília, karaokê e divisão de equipes ocorrerão das 22:00 às 00:30 na sexta-feira, seguidos por jantar e luau.<br>
        2.2.3 No sábado, após o café da manhã, haverá brincadeiras e jogos das 10:00 às 13:00 e das 14:00 às 15:30, com intervalo para almoço e lanche.<br>
    - **3 Equipe técnica**
        - **3.1 Apresentadores**<br>
        3.1.1 Os apresentadores comunicam regras, motivam, interagem com o público e garantem um evento fluido e envolvente.
        - **3.2 Animadores**<br>
        3.2.1 Os animadores devem promover um ambiente animado e alegre, contagiar o evento com confusões educativas e humor, e colaborar com o sonoplasta para manter a energia elevada.<br>
        3.2.2 O sonoplasta deve estar em contato direto com os animadores para promover músicas cristãs eletrizantes e manter a energia elevada durante todo o evento.<br>
        3.2.3 Somente o sonoplasta poderá ter acesso ao equipamento de aúdio usado no evento<br>
        3.2.4 Somente o sonoplasta poderá ativar a sirene durante o evento
        - **3.3 Juízes**<br>
        3.3.1 Os juízes devem conhecer as regras do evento e manter a imparcialidade durante todas as competições.<br>
        3.3.2 Cada evento terá uma equipe técnica composta por um juiz e um auxiliar para garantir a conformidade às regras.<br>
        3.3.3 Os pontos das brincadeiras e jogos devem ser enviados através do link.<br>
        3.3.4 O auxiliar deve portar um celular para acessar o link de pontuação.<br>
        3.3.5 As pontuações só terão validade se enviadas via link<br> 
        3.3.6 As pontuações devem ser conferidas pelo juiz e auxiliar antes do envio.<br>
        3.3.6 Os pontos serão contabilizados e informados no dia da premiação.
        - **3.4 Apoiadores**<br>
        3.4.1 Os apoiadores devem garantir a organização e preparação eficaz das brincadeiras e atividades esportivas.<br>
        3.4.2 Devem conferir equipamentos, equipes e participantes para assegurar a sua participação e envolvimento.<br>
        3.4.3 Os apoiadores devem fiscalizar pontos de falhas e levar informações relevantes aos juízes e responsáveis.<br>
        3.4.4 É fundamental que os apoiadores trabalhem de forma coesa para garantir o sucesso e a fluidez dos jogos.
    - **4 Planejamento**
        - **4.1 Confrontos - Equipes**<br>
        4.1.1 Cada equipe terá um líder (adolescente ou adulto) responsável por ajudar a equipe durante os jogos, auxiliando e moderando os participantes que excederem nas suas abordagens.<br>
        4.1.2 Cada equipe deve promover um capitão que levará as questões ao líder, que após avaliação, comunicará ao corpo técnico.<br>
        4.1.3 Cada equipe deve criar e entoar seu grito de guerra ao longo da competição e terá a oportunidade de apresentá-lo de forma criativa no final, com o melhor grito sendo pontuado.<br>
        4.1.4 As equipes serão formadas na sexta-feira após o karaokê, e todos os participantes devem ser orientados sobre este momento com clareza pelas apresentações.<br>
        4.1.5 Todos os participantes devem ser orientados sobre a importância da participação igualitária e do respeito às funções de líder e capitão dentro da equipe.
        - **4.2 Confrontos - Pontuações**<br>
        4.2.1 Critérios de Avaliação: As pontuações são atribuídas com base no grau de desafio e participação nas atividades. Atividades que incentivam maior envolvimento e exigem múltiplas habilidades recebem mais pontos.<br>
        4.2.2 Regras e Pontuações dos Jogos:<br>
            4.2.2.1 **Juntando as peças**Cada equipe terá que pegar os objetos de acordo com a forma ou cor sorteada.<br>
            4.2.2.2 **Desafio da Múmia** A equipe que completar o desafio da múmia em primeiro será pontuada.<br>
            4.2.2.3 **Bola no lençol** Os participantes deverão conduzir a bola no tecido de tnt ou lençol até o outro lado, passando por todos os obstáculos, sem pular nenhum deles e sem deixar a bola cair. Ganha a equipe que terminar primeiro chegando na vez dos participantes que iniciaram a jogada.<br>
            4.2.2.4 **Corrida** A equipe que completar a corrida em primeiro, segundo e terceiro serão pontuadas.<br>
            4.2.2.5 **Jogo da velha no chão** Cada equipe será composta por 3 jogadores. Antes de cada rodada, será decidido por par ou ímpar qual equipe jogará. Vence a equipe que formar a sequência antes. Os membros da equipe não podem dar dicas aos participantes. Apenas os jogadores selecionados podem fazer as jogadas.<br>
            4.2.2.6 **Volleyball** Cada time deve ter 6 participantes. Total de 07 pontos diretos por partida. Serão realizados 3 partidas por equipe. Cada vitória valerá apenas 10 pontos.<br>
            4.2.2.7 **Balde de Água** A equipe que completar o desafio do balde de água em primeiro, segundo e terceiro lugar serão pontuadas.<br>
            4.2.2.8 **Vestindo a camisa** Só é permitido iniciar a vestimenta da camisa após o início da música. O participante que deixar o balão cair será desclassificado. Os participantes não podem esbarrar uns nos outros.<br>
            4.2.2.9 **Desafio de Charadas** Cada equipe terá um tempo específico para resolver cada charada, O uso de celulares ou dispositivos eletrônicos é proibido durante o jogo, promovendo interação e trabalho em equipe. O objetivo é estimular o raciocínio e a criatividade dos participantes de forma divertida e dinâmica.<br>
            4.2.2.10 **Confeccionar Pipa** Cada pipa deve ter no mínimo uma rabiola de 03 metros. Cada equipe deve confeccionar mínimo de 02 pipas. Só será permitido empinar as pipas após o término da partida de vôlei e com a autorização do juiz.<br>
            4.2.2.11 **Empinar Pipa** A equipe que empinar a pipa em primeiro lugar, atingindo cerca de 10 metros de altura, será a vencedora e ganhará 50 pontos. A segunda colocada receberá 30 pontos, e a terceira, 10 pontos como reconhecimento pelo esforço na confecção.<br>
            4.2.2.12 **Grito de Guerra** A equipe que tiver o melhor, segundo melhor e terceiro melhor grito de guerra ganhará respectivamente serão pontuadas.<br>
            4.2.2.13 Todos os participantes dos jogos devem vestir roupas adequadas de banho por baixo das roupas "tradicionais", o risco de se molhar é inevitável.<br>
            4.2.2.14 A equipe que totalizar a maior pontuação ao final de todas as atividades será considerada a equipe vencedora do evento.<br>
            4.2.2.15 Para garantir a limpeza eficaz do espaço da Igreja Get-Portugal, a colaboração de todos os envolvidos no Projeto Alegria é essencial.<br>
        -  **4.3 Divulgação e Marketing**<br>
        4.3.1 As informações do evento serão compartilhadas durante os cultos.<br>
        4.3.2 Serã utilizado o sistema Infoget para comunicações internas.<br>
        4.3.3 Mensagens e atualizações serão enviadas nos grupos de WhatsApp da igreja.<br>
        4.3.4 Publicações e histórias serão feitas na página do Instagram @getsemaniportugal.<br>
        - **4.4 Sala de Jogos**<br>
        4.4.1 Só poderá participar da sala de jogos aqueles participantes que justificarem o motivo real plausivel que impossibilitam a atuação nas brincadeiras<br>
        4.4.2 Para participar da sala de jogos, é necessário passar por avaliação e autorização da equipe de coordenação local.<br>
        - **4.5 Cozinha**<br>
        4.5.1 A equipe da cozinha tem acesso exclusivo às áreas de preparação e serviço de alimentos durante o evento.<br>
        4.5.2 Apenas os integrantes listados (Osane, Vilma, Gilberti, Leli, Andrea, Lú) têm permissão para operar na cozinha.<br>
        4.5.3 A equipe da cozinha é responsável por garantir que todos os participantes sejam bem alimentados ao longo das atividades, preparando os cardápios de café da manhã, almoço e lanche conforme definido.<br>
        - **4.6 Pontuações**<br>
        4.6.1 Os participantes serão avaliados continuamente e pontuados não apenas pela capacidade de vencer, mas também pela performance e participação.<br>
        4.6.2 A premiação para o 1º lugar incluirá (medalha, troféu e a escolha de um prato pela equipe vencedora) decidido em votação.<br>
        4.6.3 A premiação para o 2º lugar incluirá medalha.<br> 
        4.6.4 A premiação para o 3º lugar incluirá um bombom e um abraço.<br>
        4.6.5 Cada equipe poderá receber apenas uma única avaliação por quesito. (Colaboração, Criatividade, Companheirismo, Participação, Comprometimento, Envolvimento, Comunicação, Empatia)<br>
        4.6.6 As avaliações serão feitas continuamente pelo corpo técnico ao longo do projeto.
        - **4.7 Formação de equipes**<br>
        4.7.1 É importante que todos os participantes estejam presentes no dia da formação das equipes.<br>
        4.7.2 Os participantes que não estiverem presentes no dia da formação das equipes, serão alocados pelo corpo técnico conforme o processo de inscrição. 























---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
