# **Cardápios - Projeto alegria**

A equipe da cozinha no Projeto Alegria Get-Portugal desempenha um papel essencial na preparação e no fornecimento de alimentos durante o evento. 

A equipe da cozinha trabalhará em conjunto para proporcionar uma experiência agradável e saborosa para todos os envolvidos.

>**Responsáveis**<br> 
>Osane - Vilma - Gilberti - Leli - Andrea - Lú - Poly - Lucilene

## Cardápio Sexta-feira 18.10.2024
!!! Lanche
    - Biscoitos, Bolos, Frutas
    - Pão com carne
    - Refrigerantes
    
## Cardápio Sabádo 19.10.2024
!!! Café da manhã
    - Café, Leite, Achocolatado, Suco
    - Bolos, Pão de sal, Pão de doce
    - Salame, Manteiga, Maionese
    - Frutas

## Cardápio Sabádo 19.10.2024
!!! Almoço
    - Strogonoff com arroz e salada
    - Refrigerante

## Cardápio Sabádo 19.10.2024
!!! Lanche
    - Refrigerante, Suco
    - Frutas

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
