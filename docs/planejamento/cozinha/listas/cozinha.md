# **Lista de Compras Cozinha**

O Cardápio a ser servido durante todo o Projeto Alegria foi pensado tanto no quesito praticidade, custo e benefício. Esta relação irá proporcionar que o evento tenha alimentos variados e acessíveis, garantindo uma alimentação saborosa e nutritiva para todos os participantes.

## Lista de Compras

| Item                        | Quantidade |
|-----------------------------|------------|
| **Sacolão**                 |            |
| - Banana                    | ??         |
| - Maça                      | ??         |
| - Abacaxi                   | ??         |
| - Melancia                  | ??         |
| - Melão                     | ??         |
| - Cebola                    | ??         |
| - Alho                      | ??         |
| - Alface                    | ??         |
| - Tomate                    | ??         |
| **Padaria**                 |            |
| - Biscoitos                 | ??         |
| - Bolos                     | ??         |
| - Pão de doce               | ??         |
| - Pão de sal                | ??         |
| - Salame                    | ??         |
| - Manteiga                  | ??         |
| - Pó de café                | ??         |
| **Supermercado**            | ??         |
| - Maionese                  | ??         |
| - Achocolatado              | ??         |
| - Colorau                   | ??         |
| - Arroz                     | ??         |
| - Batata palha              | ??         |
| - Creme de leite sem lactose| ??         |
| **Açougue**                 |            |
| - Carne de pernil           | ??         |
| - Peito de frango sem osso  | ??         |
| **Descartáveis**            |            |
| - Guardanapo                | ??         |
| - Prato de isopor ou cumbuca| ??         |
| - Colher                    | ??         |
| - Copos 100 ml              | ??         |

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte - MG, 31560-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
