# **Segundo e terceiro colocado** 

## Detalhes Adicionais
Os participantes serão observados a todo momento e pontuados não apenas pela capacidade de vencer, mas também pela performance e participação.

## Premiação

!!! danger "**2º Lugar:**"
    - Medalha

!!! note "**3º Lugar:**"
    - Bombom e um abraço

**Local do Prêmio** _O prêmio será entregue na Igreja batista - Getsêmani Missão Portugal._

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
