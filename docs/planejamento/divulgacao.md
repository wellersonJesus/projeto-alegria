# **Divulgação e Marketing**

Utilizaremos diversos canais para garantir que a divulgação alcance todos os membros e interessados. A equipe técnica foi dividida em cinco grupos, e cada grupo deverá gravar um vídeo curto e criativo para ser apresentado em todos os canais de divulgação da igreja. Além disso, toda a equipe deverá se reunir para gravar um vídeo conjunto.

## Equipe técnica divulgação e marketing 
>**Pra Glaúcia** - Maria Alice - Welbert<br> 
>**Thais** - Osane - Luana<br>
>**Gabriela** - Bel - Vilma<br>
>**Jeferson** - Pr. Cláudio - Gisele<br> 
>**Wilson** - Joelson - Arlem<br>

## Canais de Divulgação
- **Cultos:** Informações serão compartilhadas durante os cultos.
- **Infoget:** Utilizaremos o sistema Infoget para comunicações internas.
- **Grupos da Igreja:** Mensagens e atualizações nos grupos de WhatsApp da igreja.
- **Instagram:** Publicações e histórias na nossa página do Instagram [@getsemaniportugal](https://www.instagram.com/getsemaniportugal/).

!!! success "**Cantina Especial de Arrecadação**"
    - Festival de Caldos (banda dos jovens)
    - Sábado, 20.07.2024 - Valor: R$ 8,00
    - Responsáveis pela Cantina: Toda Equipe do Projeto Alegria

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>


