# **Sala de Jogos**

A sala de jogos oferecerá um ambiente descontraído e tranquilo para os participantes que desejarem relaxar, interagir e se divertir. Os jogos do projeto incluem uma variedade de atividades divertidas para os participantes desfrutarem durante o evento.

>**Responsáveis**<br>
>Lú do Junão - Lucíola - Janyne

## Quais serão os jogos disponíveis ?
!!! Jogos
    - Uno<br> 
    - Dama<br> 
    - Lápis de cor<br> 
    - Quebra-cabeça<br> 
    - Boneca<br>
    - Papel A4<br> 
---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>

