# **Lista de Materiais**

A lista de materiais a ser adquirida para realizar os jogos e brincadeiras do Projeto Alegria foi pensada considerando praticidade, custo e benefício. Esta relação visa garantir que o evento ocorra adequadamente, permitindo que as brincadeiras e jogos fluam devidamente.

## Lista de Compras

| **Item**                                 | **Quantidade**   | **Responsável**                    |
|------------------------------------------|------------------|------------------------------------|
| Bacias grandes                           | 3                | Joelson                            |
| Cartões para sortear formas e cores      | 100              | Joana                              |
| Rolos de papel                           | 9                | Wellerson                          |
| TNT vermelha (5 metros)                  | 1                | Cláudia                            |
| TNT verde (5 metros)                     | 1                | Cláudia                            |
| TNT azul (5 metros)                      | 1                | Cláudia                            |
| Bola leve vermelha                       | 1                | Andréia                            |
| Bola leve verde                          | 1                | Andréia                            |
| Bola leve azul                           | 1                | Andréia                            |
| Pneus                                    | 9                | Wellerson 2, Thais 4, Jeffinho 3   |
| Cones                                    | 9                | Welbert                            |
| Cordas                                   | 50 metros        | Fernando                           |
| Corrente plástica                        | 50 metros        | Joelson 27m, Fernando 25m          |
| Fitas zebradas                           | 2                | ok                                 |
| Fita crepe grande                        | 2                | ok                                 |
| Camisas vermelhas                        | 3                |                                    |
| Camisas verde                            | 3                |                                    |
| Camisas azuis                            | 3                |                                    |
| Bola de vôlei                            | 1                | Wilson Osane                       |
| Rede de vôlei                            | 1                | Wilson Osane                       |
| Baldes                                   | 6                | ok                                 |
| Camisa GG ou Extra G vermelha            | 1                | Thais                              |
| Camisa GG ou Extra G verde               | 1                | Thais                              |
| Camisa GG ou Extra G azul                | 1                | Thais                              |
| Pacote de balão vermelho                 | 1                | Cris Barroso                       |
| Pacote de balão verde                    | 1                | Cris Barroso                       |
| Pacote de balão azul                     | 1                | Cris Barroso                       |
| Cartões mínimo 10 charadas diferentes    | 03               | Joana                              |
| Pranchetas e canetas                     | 02               | ok, Joelson                        |
| Pacote de papéis coloridos               | 1                |                                    |
| Rolos de fita                            | 3                |                                    |
| Cornetas                                 | 3                | Cláudia                            |
| Apitos                                   | 8                | ok, Joelson                        |
| Kits para confeccionar pipas (vermelho)  | 2                | Fernando                           |
| Kits para confeccionar pipas (verde)     | 2                | Fernando                           |
| Kits para confeccionar pipas (azul)      | 2                | Fernando                           |
| Carretel de linha simples para pipa      | 3                | David                              |

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d
