# **Pontuações**

## Critérios de Avaliação

Buscamos equilibrar as atividades, garantindo pontuações proporcionais ao desafio e participação. 

Atividades que exigem múltiplas habilidades ganharam mais pontos. As etapas finais, como a "Apresentação do Grito de Guerra", tiveram pontuações mais altas para motivar as equipes.

| Desafios                   | 1º Lugar | 2º Lugar  | 3º Lugar  | Juiz       | Auxiliares                                  |
|----------------------------|----------|-----------|-----------|------------|---------------------------------------------|
| Grito de Guerra            | 10 pontos| 0         | 0         | Laura      | Cida, David                                 |
| Juntando as Peças          | 10 pontos| 07 pontos | 05 pontos | Joana      | Cida, Laura                                 |
| Desafio da Múmia           | 10 pontos| 07 pontos | 05 pontos | Cida       | Joana, Bel                                  |
| Jogo da Velha no Chão      | 05 pontos| 0         | 0         | Jeferson   | Menezes, Fernando                           |
| Vestindo a Camisa          | 10 pontos| 07 pontos | 05 pontos | Bel        | Menezes, Welbert                            |
| Bola no tecido             | 10 pontos| 07        | 05        | Fernando   | Joana, Menezes                              |
| Desafio de Charadas        | 15 pontos| 0         | 0         | Joana      | Jeferson, David                             |
| Volleyball                 | 10 pontos| 0         | 0         | Welbert    | Menezes, Arlem                              |
| Confeccionar Pipa          | 10 pontos| 0         | 0         | Jeferson   | David, Fernando                             |
| Empinar a Pipa             | 50 pontos| 30 pontos | 10 pontos | David      | Wellerson, Arlem, Jeferson, Welbert, Menezes|
| Corrida com Obstáculos     | 20 pontos| 10 pontos | 05 pontos | Arlem      | Welbert, Fernando                           |
| Balde de Água              | 30 pontos| 20 pontos | 10 pontos | Wellerson  | Welbert, Menezes                            |

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
