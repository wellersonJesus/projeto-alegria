# **Brincadeiras**

As brincadeiras formam o núcleo de competição entre as equipes. Abaixo estão apresentados os nomes das brincadeiras, suas explicações, regras e objetivos.

>**Responsáveis técnicos**  
>Wellerson - Isabel - Thais - Arlen - Joana Darc 

> **Importante**
_A equipe Kids se dividirá em acompanhar os jogos dentro do templo e as atividades programadas para o infantil no lado de fora no espaço memórias
Atividades distribuídas por horário e local – definido no material._  

---

## <span style="color: #c46564;">** 1º Desafio - Grito de Guerra <br> Faixa etária - Toda equipe**</span>

**Local** Templo  

**Hora** 10:30 – 10:45   

**Pontuação** 5, 7 e 10 pontos   

**Explicação:** O "Grito de Guerra" é uma apresentação em que a equipe se reúne para criar e executar um grito ou canto que representa o espírito e a identidade do grupo. Esta atividade promove a união, a criatividade e o entusiasmo entre os membros da equipe.

**Regra:** Cada equipe deve criar um grito de guerra original e executá-lo em frente aos juízes. A apresentação deve durar entre 1 a 3 minutos e pode incluir elementos como música, coreografia e adereços. O respeito aos limites de tempo e a inclusão de todos os membros da equipe são critérios importantes para a avaliação.

**Objetivo:** O objetivo do "Grito de Guerra" é fomentar a criatividade, a colaboração e o espírito de equipe. Além disso, visa motivar os participantes, reforçar a identidade do grupo e criar um ambiente de competição saudável e divertida.

---

## <span style="color: #c46564;">** 2º Desafio - Juntando as peças <br> Faixa etária - 2 a 5 anos**</span>

**Local** Templo  

**Hora** 10:30 – 10:45   

**Pontuação** 5, 7 e 10 pontos   

**Explicação:** A equipe colocará vários objetos (recicláveis) juntos numa bacia, um representante de cada equipe terá que pegar os objetos de acordo com a forma ou cor sorteada.  

**Regra:** Os objetos devem ser de tamanhos, cores e formas bem visíveis e acessível ás crianças. Se for selecionado cores deverá ser somente daquela cor, se for forma somente daquela forma. Ganha a equipe que terminar primeiro.  

**Objetivo:** Promover a coordenação motora, habilidades sequenciais e atenção.  

---

## <span style="color: #c46564;">** 3º Desafio - A Múmia <br> Faixa etária - 6 a 8 anos**</span>

**Local** Templo  

**Hora** 10:45 ÀS 11:00    

**Pontuação** 5, 7 e 10 pontos  

**Explicação:** Em equipes de três, os participantes têm um rolo de papel higiênico. Um dos membros da equipe é "a múmia" e deve ser completamente envolto pelo papel. A equipe que terminar primeiro vence.

**Regra:** Cada equipe tem três minutos para completar o desafio. O juiz determina a equipe vencedora com base na criatividade e eficiência.

**Objetivo:** Promover criatividade, cooperação e trabalho em equipe.

---

## <span style="color: #c46564;">** 4º Desafio – Jogo da velha no chão <br> Faixa etária - 9 a 13 anos**</span>

**Local** frente ao Espaço Gourmet  

**Hora** 11:00 – 11:30    

**Pontuação** 0 e 5 pontos   

**Explicação:** O jogo será realizado entre duas equipes. O tabuleiro do jogo da velha será marcado no chão com fita crepe, e cada equipe posicionará seus participantes nas marcações. Ganha a equipe que completar uma sequência primeiro.

**Regra:** Cada equipe será composta por 3 jogadores. Antes de cada rodada, será decidido por par ou ímpar qual equipe jogará. Vence a equipe que formar a sequência antes. Os membros da equipe não podem dar dicas aos participantes. Apenas os jogadores selecionados podem fazer as jogadas.

**Objetivo:** Estimular a atenção e concentração e desenvolver a sequência lógica de cores ou formas.

---

## <span style="color: #c46564;">** 5º Desafio – Vestindo a Camisa**<br>**Faixa etária: 14 a 17 anos**</span>

**Local** Estacionamento 

**Hora** 11:30 à 11:45    

**Pontuação** 5,7 e 10 pontos  

**Explicação:** 
Três rodadas com um representante por equipe: segurem balão e camisa, ao som da sirene, o participante que vestir a camisa primeiro vence; caso contrário, quem estiver com a camisa melhor vestida ganha a rodada.

**Regras:** Só é permitido iniciar a vestimenta da camisa após o início da música. O participante que deixar o balão cair será desclassificado. Os participantes não podem esbarrar uns nos outros.

**Objetivo:** 
Vestir a camisa completamente, demonstrando coordenação e equilíbrio.

---

## <span style="color: #c46564;">** 6º Desafio – Charadas <br> Faixa etária - 18 a 25 anos**</span>

**Local** Templo

**Hora** 13:30 – 14:00    

**Pontuação** 15 pontos 

**Explicação:** Os participantes são divididos em equipes e devem resolver uma série de charadas verbais ou visuais. Cada charada pode envolver enigmas, adivinhações ou representações físicas.

**Regra:** Cada equipe terá um tempo específico para resolver cada charada, O uso de celulares ou dispositivos eletrônicos é proibido durante o jogo, promovendo interação e trabalho em equipe. O objetivo é estimular o raciocínio e a criatividade dos participantes de forma divertida e dinâmica.

**Objetivo:** Promover o pensamento rápido, a criatividade e a comunicação eficaz entre os participantes. Além disso, incentivar o trabalho em equipe e a colaboração para alcançar o objetivo comum de resolver as charadas.

---

## <span style="color: #c46564;">** 7º Desafio – Volleyball <br> Faixa etária - 9 a 25 anos**</span>

**Local** Estacionamento

**Hora** 14:00 as 15:00    

**Pontuação** 10 pontos por vitória

**Regra:** Cada time deve ter 6 participantes. Total de 07 pontos diretos por partida. Serão realizados 3 partidas por equipe. Cada vitória valerá apenas 10 pontos.  

**Explicação:** O jogador não poderá encostar na rede, não poderá pisar na linha quando for sacar e os pontos serão corridos.

**Objetivo:** Promover competitividade entre as equipes.

---

## <span style="color: #c46564;">** 8º Desafio - Confeção da Pipa <br> Faixa etária - Toda equipe**</span>

**Local** Espaço Gourmet /Estacionamento

**Hora** 14:00 as 15:00    

**Pontuação** Confecção 10 pontos (se a pipa não subir somente ganha a confecção)

**Regra:** Cada pipa deve ter no mínimo uma rabiola de 3 metros. Cada equipe deve confeccionar mínimo de 02 pipas. Só será permitido empinar as pipas após o término da partida de vôlei e com a autorização do juiz.

**Critérios para a confecção das pipas:**  
> Formato: corte e envergadura bem definidos.  
> Barriga: tamanho mediano.  
> Acabamento: pontas bem finalizadas.  
> Barbela: posicionada próxima à ponta da envergadura.  
> Rabiola: com três metros de comprimento, sendo pelo menos um metro de parte cheia.  
> Bambulins: de tamanho mediano.  

**Explicação:** Será entregue o material necessário para a confecção das pipas. Cada equipe só poderá começar a fazer a pipa somente após o sinal dado pelo juiz. A equipe que conseguir empinar a pipa em primeiro lugar a uma altura de aproximadamente 10 metros é considerada a equipe vencedora.

**Objetivo:** Todos da equipe devem e podem participar, sendo importante a organização e a colaboração dos integrantes tanto na confecção quanto na execução no campo para empinar a pipa.


## <span style="color: #c46564;">** 9º Desafio - Empinar pipa <br> Faixa etária - Toda equipe**</span>

**Local** Estacionamento

**Hora** 15:00 as 15:20

**Pontuação** 10, 30 e 50 pontos

**Regra:** Somente após o término da partida de vôllêybol e a sinalização dado pelo juiz, as equipes poderão ir ao campo para empinar suas pipas juiz. 

**Explicação:** A equipe que empinar a pipa em primeiro lugar, atingindo cerca de 10 metros de altura, será a vencedora e ganhará 50 pontos. A segunda colocada receberá 30 pontos, e a terceira, 10 pontos como reconhecimento pelo esforço na confecção.

**Objetivo:** Todos da equipe devem e podem participar, sendo importante a organização e a colaboração dos integrantes tanto na confecção quanto na execução no campo para empinar a pipa.

---

## <span style="color: #c46564;">** 10º Desafio – Corrida com obstáculos<br> Faixa etária - 9 a 13 anos**</span>

**Local** Estacionamento

**Hora** 15:30 – 15:45

**Pontuação** 20, 10 e 5 pontos

**Explicação:** Os participantes competem em uma corrida de velocidade em uma pista determinada. O objetivo é chegar primeiro à linha de chegada.

**Regra:** A corrida é realizada em uma pista específica com marcações claras de largada e chegada. É importante seguir as instruções do árbitro para um início limpo e justo. Vence o participante que cruzar a linha de chegada em primeiro lugar.

**Objetivo:** Estimular a competição saudável, a velocidade e a coordenação entre os participantes da faixa etária de 9 a 13 anos.

---

## <span style="color: #c46564;">** 11º Desafio - Bola no tecido <br> Faixa etária - 6 a 9 anos**</span>

**Local** Estacionamento  

**Hora** 15:30 ÀS 15:45    

**Pontuação** 5, 7 e 10 pontos  

**Explicação:** As equipes participantes estarão em filas, de um lado, e do outro lado estará integrantes de cada equipe. Ao som do Apto os dois primeiros participantes deverão conduzir a bola em um lençol, Após finalizar o percurso, entregar o lençol com a bola para os próximos participantes da fileira. Se a bola cair, eles deverão recomeçar todo percurso novamente., passando por obstáculos ( pneus, cones, cordas em zig-zag) entregar o lençol com a bola para os participantes que estão na outra ponta que farão o mesmo percurso ao contrário.    

**Regra:** Os participantes deverão conduzir a bola no tecido de tnt ou lençol até o outro lado, passando por todos os obstáculos, sem pular nenhum deles e sem deixar a bola cair. Ganha a equipe que terminar primeiro chegando na vez dos participantes que iniciaram a jogada.

**Objetivo:** Estimular a velocidade, coordenação, equilíbrio e trabalho em equipe.

---

## <span style="color: #c46564;">** 12º Desafio – Balde de água <br> Faixa etária - 14 a 17 anos**</span>

**Local** Estacionamento

**Hora** 15:45 - 16:00

**Pontuação** 30,20 e 10 pontos

**Explicação:** Todos os integrantes da equipe devem se sentar formando uma única fila, cada participante em uma cadeira. O líder entregará um balde cheio de água ao primeiro integrante da equipe e um balde vazio ao segundo integrante. Quando for dado o sinal de início, cada participante deve permanecer sentado e com os olhos fechados passar o balde cheio para trás por cima da cabeça até o próximo integrante.

**Regra:** O líder é responsável por pegar o balde vazio e entregá-lo ao próximo participante na fila para que o processo continue. Este fluxo deve ser repetido até que o balde tenha passado por todos os integrantes da equipe. Os participantes não podem levantar da cadeira até que o juiz finalize a brincadeira.

**Objetivo:** Transferir a água do balde cheio para o balde vazio.

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_  
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
