# **Equipes, Jogos e Brincadeiras**

Para fluir todo projeto com clareza e justiça foi pensado em alguns pontos importantes de atores junto as equipes para que os jogos e brincadeiras fluam de forma coesa e objetiva.

## Qual é o papel do ator Líder de equipe ?
Para garantir a organização eficiente das equipes e a participação igualitária de todos, cada equipe será composta de um líder (adolescente - adulto). O papel do líder e ajudar a equipe durante os jogos auxiliando e retendo os participantes que excederem nas suas abordagens. Os lideres devem ser orientados quanto a isso e ter total ciência da sua função junto a equipe. 

## Qual é o papel do ator Captão de equipe ?
Cada equipe deverá promover um capitão do time. O papel do captão do time e levar as questões junto ao líder, que somente então após densa avaliação levar ao corpo tecnico a devida abordagem. O corpo tecnico só irá poder conversar com o líder responsável por cada equipe


## Grito de guerra 
Cada equipe deverá criar seu _Grito de guerra_. O grito de guerra deve ser entoado ao longo de toda competição para motivar os membros durante as brincadeiras e jogos. 

Após o desafio final, cada equipe terá a oportunidade de apresentar seu grito de forma criativa e performatica bem animada. O melhor grito será pontuado

---

## Formação de equipes

- As equipes serão formadas na sexta-feira, logo apos o "caraoke" que acontecerá no tempo principal. 
- Os apresentadores apresentarão com clareza as informações devidas sobre o projeto. 
- Somente após as equipes formadas todos apoiadores do projeto poderá atuar em seus devidos papeis.

**Importante** É importante que todos os participantes estejam presentes no dia da formação das equipes. Aqueles que não estiverem serão alocados pela equipe técnica conforme o processo de inscrição.

## <span style="color: red;">**Equipe Vermelha**</span>

| Faixa 2 a 5 anos   | Faixa 6 a 8 anos   | Faixa 9 a 13 anos   | Faixa 14 a 17 anos   | Faixa 18 a 25 anos   |
|--------------------|--------------------|---------------------|----------------------|----------------------|
| AAAA               | BBBB               | CCCC                | DDDD                 | VCCCC                |

>**Líder**: Pedro Barroso  
>**Captão**: A definir 
---

## <span style="color: green;">**Equipe Verde**</span>

| Faixa 2 a 5 anos   | Faixa 6 a 8 anos   | Faixa 9 a 13 anos   | Faixa 14 a 17 anos   | Faixa 18 a 25 anos   |
|--------------------|--------------------|---------------------|----------------------|----------------------|
| AAAA               | BBBB               | CCCC                | DDDD                 | VCCCC                |

>**Líder**: Fernanda  
>**Captão**: A definir  
---

## <span style="color: blue;">**Equipe Azul**</span>

| Faixa 2 a 5 anos   | Faixa 6 a 8 anos   | Faixa 9 a 13 anos   | Faixa 14 a 17 anos   | Faixa 18 a 25 anos   |
|--------------------|--------------------|---------------------|----------------------|----------------------|
| AAAA               | BBBB               | CCCC                | DDDD                 | VCCCC                |

>**Líder**: Diogo  
>**Captão**: A definir 

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>

