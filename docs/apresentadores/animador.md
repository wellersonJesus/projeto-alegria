# **Animadores**

Os animadores atuarão diretamente na dinâmica das atividades jogos e brincadeiras, entre as equipes, promovendo um ambiente animado e alegre. São responsáveis por enriquecer a interação entre os participantes e manter o clima de animação em alta durante todas as atividades.

>**Animadores**<br> 
>Pr. Cláudio - Joelson

## De fato, com o que os animadores devem se preocupar ?

  Os animadores devem se preocupar em contagiar todo evento "causando" com conciência uma bagunça generalizada, confusões inteligentes educativas para agitar todo evento, é seu dever estar cientes das orientações gerais para poder auxiliar no que for preciso no quesito informação. 
  
  Devem levar em consideração o lado comico e descontraído o evento. Para melhor organização e entreterimento, os animadores serão os únicos "atores" que poderão ter acesso aos microfones junto aos apresentadores, de fato podendo interrope-los a qualquer momento, porém com a devida cautela.
  
## Sonoplasta do projeto  

O sonoplasta do projeto deve estar em contato direto com os animadores. É responsabilidade dos animadores direcionar e ajudar o sonoplasta no quesito musica, durante todo projeto. 

O sonoplasta deve ter liberdade de promover músicas cristas eletrizantes para manter a energia "elevada" durante todo evento. 

>**Sonoplasta**<br>
>Wilson


---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
