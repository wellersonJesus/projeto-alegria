# **Apoiadores**

Para garantir o sucesso e a fluidez dos jogos, é fundamental a criação de uma equipe de apoiadores competente e coesa

>**Apoiadores**<br> 
>Ana Paula Muniz - Ana Paula - Fernando - Claudia - Rogério - Joana Darc - Joana D'arc - Junão - Vilma - Lilian - Lucas - Agostinho

## Fortalecendo a Organização dos Jogos

Este corpo técnico irá desempenhar o papel de organização no desenvolvimento eficaz das atividades esportivas e lúdicas propostas durante o evento. Organizar e preparar as bincadeiras, conferir equipamentos, equipes e participantes quanto a sua participação e envolvimento, fiscalizar pontos de falhas e levar informações relevantes aos juízes e responsáveis. 

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
