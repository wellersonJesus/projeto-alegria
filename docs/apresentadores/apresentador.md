# **Apresentadores**

Os apresentadores desempenharão o papel de comunicação e envovlvimento, serão os responsáveis por explicar regras, motivar os participantes, interagir com o público e criar uma atmosfera envolvente para todos os envolvidos.

>**Apresentadores**<br> 
>Wilson - Gisele

## Quais são as princípais tarefas dos apresentadores:

. Apresentar as atividades do evento de forma clara e envolvente.<br>
. Manter os participantes informados sobre os detalhes e horários das atividades.<br>
. Garantir o fluxo do evento, tornando suave e descontraído para os envolvidos.

Os apresentadores desempenham um papel importantíssimo para o sucesso geral do evento, assegurando que cada momento seja aproveitado ao máximo pelos participantes.


---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
