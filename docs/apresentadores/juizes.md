# **Juízes Brincadeiras e jogos**

A função do juiz é conhecer as regras do evento e manter a imparcialidade. Os juizes devem garantir a estrita conformidade as regras de cada jogo assegurando imparcialidade e integridade nas competições. Para cada evento será designada uma equipe técnica para acompanhamento. Está equipe tecnica será composta de (01 juiz e 01 auxiliar). 

>**Juízes**<br>
> Laura - Joana - Cida - Jeferson - Bel - Fernando - Welbert - David - Arlem - Wellerson

*Os juizes e auxiliares deverão marcar os pontos das brincadeiras e jogos atráves do link que será disponibilizado somente no dia das competições*

---

# Importante

- O auxiliar deve portar celular<br> 
- As pontuações e informações só terá validade via link<br>
- É dever do juíz conferir junto ao auxiliar todos os pontos antes do envio

*OS pontos serão contabilizados e informado no dia da premiação* <br>
_[Link envio pontuação equipe]()_

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
