# **Cronograma**

## Cronograma 18.10.2024 (Sexta-feira)
!!! Sexta-feira   
    - **Recepção identificação - Montagem barracas** _21:00_ 
    - **Vigília:** _Banda - Agápe - 21:00 - 22:00_ 
    - **Karaokê:** _Banca de Jurados - 22:00 - 23:00_ 
    - **Divisão de Equipes:** - _23:30 - 00:00_ 
    - **Prova relâmpago:** _Desafio de Charadas - 00:00 - 00:30_
    - **Jantar, fogueira e "causos:"** - _00:50 ..._ 

## Cronograma 19.10.2024    
!!! Sábado 
    - **Espaço gourmet:** _Café da manhã - 07:00 - 09:00_
    - **Entrada templo:** _Recepção - 09:00 - 10:00_
    - **Templo Palavra:** - _09:30 - 10:00_
    - **Estacionamento - quadra:** _Brincadeiras e jogos - 10:00 - 13:00_ 
        
!!! Almoço         
    - **Espaçõ gourmet:** _Livre - 13:00 - 14:00_ 
    
!!! Atividades             
    - **Estacionamento quadra:** _Brincadeiras e jogos - 14:00 - 15:30_
    - **Lanche - Encerramento:** - _15:30 - 16:00_ 
            
---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>
