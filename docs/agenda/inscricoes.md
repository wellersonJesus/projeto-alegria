# **Inscrições**

## <span style="color: #c46564;">**Período de inscrições**</span>  
> **Inscrições abertas** - _10.09.2024 às 18:00 horas_    
> **Inscrições encerradas** - _08.10.2024 às 18:00 horas_


>**Responsáveis**<br> 
>Luciana - Giceli - Cristiane

A inscrição é obrigatória e será utilizada para o controle de entrada e check-in de todas as crianças, juniores, adolescentes e jovens, sendo o único meio de garantir sua participação ativa no projeto Alegria Get-Portugal. 

É fundamental priorizar o suporte operacional e logístico das inscrições. A preparação do local e a organização do processo de inscrição são essenciais para o sucesso do projeto.

## Acesse o site  
_Para mais informações acesse nosso site_  
[Site](https://sites.google.com/view/projeto-alegria/in%C3%ADcio)  

## Inscreva-se aqui
*Clique no link para inscrever* <br>
_[Inscrição](https://forms.gle/CR9ZMEpokgsWqLfj8)_


*Ou use o Qr-Code a seguir*
![Alt text](QRCode_09.09.24.png) 

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/)_</sub>


