# **Agenda**

## Acompanhe nosso calendario 2024

| Data hora        | Evento                              |Detalhes                               |
|------------------|-------------------------------------|---------------------------------------|
| ✅ 03.06 - 20:00  |  1º Reunião de alinhamento          | Definição documentação projeto        |
| ✅ 17.06 - 20:00  | 2º Reunião de alinhamento           | Apr. Docmentação - Cantina Especial   |
| ✅ 08.07 - 20:00  | 3º Reunião de alinhamento           | Alinhamento                           |
| ✅ 05.09 - 20:00  | 4º Runião geral                     | Pequenos Ajustes reunião geral        |
| ✅ 26.09 - 20:15  | 5º Runião equipe brincadeiras       | Definição locais brincadeiras em loco |
| ✅ 10.09 - 18:00  | Inscrições - Abertas                | Inicio das incrições                  |
| ✅ 08.10 - 22:00  | Inscrições - Encerramento           | Encerramento das inscrições           |
| ✅ 18.10 - 18:30  | Projeto alegria                     | Data início                           |
| ✅ 19.10 - 16:00  | Projeto alegria                     | Data encerramento                     |


---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>

