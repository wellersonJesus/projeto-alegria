## **[Bem-vindo ao Projeto Alegria Get-Portugal](https://sites.google.com/view/projeto-alegria/in%C3%ADcio)**

## O que é o Projeto Alegria?
O **Projeto Alegria** é uma iniciativa cristã da igreja Get-Portugal focada em promover o evangelismo e o carácter firmado na palavra de Deus, através de brincadeiras, jogos e atividades culturais, sociais e educacionais. Nosso objetivo é unir membros da igreja, crianças jovens e adolescentes de 2 a 25 anos para celebrar a cristo e construir uma comunidade mais firmada no evangelho.

<div style="text-align: lefth;">
    <span style="color: #25736d; display: inline-block; text-align: left; font-family: 'Playwrite USA Traditional', sans-serif; font-size: 16px; font-style: italic;">
        "...Vós me ensinareis o caminho da vida, há abundância de alegria junto de vós, <br>e delícias eternas à vossa direita."<br>Salmos 16:11
    </span>
</div>

## Como posso participar do projeto?
Você pode participar do Projeto Alegria de diversas formas! Inscreva-se como voluntário, participe das nossas atividades e eventos, ou contribua com suas ideias e talentos. Visite a nossa seção "[Informações Gerais](informacoes/informacoes_geral.md)" para saber mais sobre as oportunidades e regras disponíveis.

---

_[Projeto Alegria Get-Portugal](https://www.instagram.com/getsemaniportugal/)_  _"Uma igreja que se relaciona e se importa"_<br>
<sub>📌 _[Av. Portugal, 300 - Jardim Atlântico, Belo Horizonte, Brazil 31550-000](https://www.google.com.br/maps/place/Av.+Portugal,+300+-+Santa+Amelia,+Belo+Horizonte+-+MG,+31560-000/@-19.8401871,-43.9935806,17z/data=!3m1!4b1!4m6!3m5!1s0xa691b783e6ad1f:0x25792e4ecacab97d!8m2!3d-19.8401871!4d-43.9910057!16s%2Fg%2F11c4yz9b7l?entry=ttu)_</sub>

